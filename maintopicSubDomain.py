import nltk

from elasticsearch import Elasticsearch
from nltk.collocations import *
from utils import Utils


# Gets the bigrams of a given "freq" in a given "text"
def get_bigrams(text, freq):
    bigram_measures = nltk.collocations.BigramAssocMeasures()
    result_tokens = es.indices.analyze(index="nested_affiliations", analyzer="lookinlabs4hal_analyzer", body=text)
    tokens = []
    for r in result_tokens["tokens"]:
        tokens.append(r["token"])
    finder = BigramCollocationFinder.from_words(tokens)
    if freq is not None:
        finder.apply_freq_filter(freq)
    bigrams = finder.nbest(bigram_measures.likelihood_ratio, 200)
    return bigrams

# Gets the trigrams of a given "freq" in a given "text"
def get_trigrams(text, freq):
    trigram_measures = nltk.collocations.TrigramAssocMeasures()
    result_tokens = es.indices.analyze(index="nested_affiliations", analyzer="lookinlabs4hal_analyzer", body=text)
    tokens = []
    for r in result_tokens["tokens"]:
        tokens.append(r["token"])
    finder = TrigramCollocationFinder.from_words(tokens)
    if freq is not None:
        finder.apply_freq_filter(freq)
    trigrams = finder.nbest(trigram_measures.likelihood_ratio, 150)
    return trigrams

# Return a list with the different words of a list of trigrams compared to a given bigram
def get_differences(bigram, ltrigrams):
    differences = []
    for t in ltrigrams:
        if t[0] not in bigram:
            differences.append(t[0])
        if t[1] not in bigram:
            differences.append(t[1])
        if t[2] not in bigram:
            differences.append(t[2])
    return differences

# Removes the duplicates of a list of bigrams
def filter_dup_bigrams(lbigrams):
    items = set()
    result = []
    for b in lbigrams:
        temp = b[0] + ' ' + b[1]
        if b not in result and temp not in items:
            items.add(b[0] + ' ' + b[1])
            items.add(b[1] + ' ' + b[0])
            result.append(b)
    return result

# Removes the bigrams of a list of bigrams that contains any keywords of a list of keywords
def filter_bigrams_by_keywords(lbigrams, lkeywords):
    result = []
    for b in lbigrams:
        add = False
        for k in lkeywords:
            if k["key"] in b:
                add = True
                break
        if add:
            result.append(b)
    return result

def get_trigrams_sub_domain(domain, completeDomText) :
    bigrams = get_bigrams(completeDomText, 3)
    trigrams = get_trigrams(completeDomText, 2)
    map = {}
    for b in bigrams:
        entry = []
        if b in map:
            entry = map[b]
        for t in trigrams:
            if b[0] in t and b[1] in t:
                entry.append(t)
        if len(entry) > 0:
            map[b] = entry

    second_map = {}
    for key in map:
        values = map[key]
        words = get_differences(key, values)
        entry = [[key[0], key[1]]]
        found = False
        for w1 in words:
            for w2 in words:
                if len(w1) != len(w2):
                    dis = utils.levenshtein_distance(w1, w2)
                    d = dis["distance"]
                    if d <= 3:
                        if len(w1) > len(w2):
                            entry[0].append(w1)
                        else:
                            entry[0].append(w2)
                    found = True
            if found is True:
                break

        if len(entry[0]) > 2:
            second_map[key] = entry
        else:
            second_map[key] = values

    maintopic_data = {}
    maintopic_data["topic"] = []
    maintopic_data["key"] = domain

    added_bigrams = []
    added_trigrams = []
    count = 0
    for b in bigrams:
        if b in second_map:
            t = second_map[b][0]
            exist_similar = False
            for at in added_trigrams:
                coincidences = 0
                if t[0] in at:
                    coincidences += 1
                if t[1] in at:
                    coincidences += 1
                if t[2] in at:
                    coincidences += 1
                if coincidences >= 2:
                    exist_similar = True
                    break

            if t not in added_trigrams and b not in added_bigrams and not exist_similar:
                maintopic_data["topic"].append(t[0] + ' ' + t[1] + ' ' + t[2])
                added_bigrams.append(b)
                added_trigrams.append(t)
                count += 1

    if count < 10:
        for b in bigrams:
            if count == 10:
                break
            if b not in added_bigrams:
                maintopic_data["topic"].append(b[0] + ' ' + b[1])
                added_bigrams.append(b)
                count += 1
    return maintopic_data

es = Elasticsearch()

utils = Utils(es)

aff = utils.get_affiliations()

for u in aff:
    # id_aff = u["_id"].replace("affiliation_", "")
    affiliationId = u["_id"]
    result = es.get(index="nested_affiliations", doc_type="nestedAffiliations", id= affiliationId)
    completeText = ""
    dictDomain = {}
    #keywords = utils.get_keywords("user_" + userId, 20);
    if "pubs" in result["_source"] :
        if len(result["_source"]["pubs"]) > 0:
            for publication in result["_source"]["pubs"]:
                title_fr = publication["title_fr"]
                title_en = publication["title_en"]
                abstract_fr = publication["abstract_fr"]
                abstract_en = publication["abstract_en"]
                completeText += title_fr + ' ' + title_en + ' ' + abstract_fr + ' ' + abstract_en + ' '
                for domain in  publication["hal_domains"] :
                    splitdomain = domain["title"].split("/")
                    if splitdomain[0] in dictDomain :
                        completeDomText = dictDomain[splitdomain[0]]
                        dictDomain[splitdomain[0]] = completeDomText + ' ' + title_fr + ' ' + title_en + ' ' + abstract_fr + ' ' + abstract_en + ' '
                    else :
                        dictDomain[splitdomain[0]] = title_fr + ' ' + title_en + ' ' + abstract_fr + ' ' + abstract_en + ' '
        bigrams = get_bigrams(completeText, 3)
        trigrams = get_trigrams(completeText, 2)
        bigrams = filter_dup_bigrams(bigrams)
        #bigrams = filter_bigrams_by_keywords(bigrams, keywords)

        map = {}
        for b in bigrams:
            entry = []
            if b in map:
                entry = map[b]
            for t in trigrams:
                if b[0] in t and b[1] in t:
                    entry.append(t)
            if len(entry) > 0:
                map[b] = entry

        second_map = {}
        for key in map:
            values = map[key]
            words = get_differences(key, values)
            entry = [[key[0], key[1]]]
            found = False
            for w1 in words:
                for w2 in words:
                    if len(w1) != len(w2):
                        dis = utils.levenshtein_distance(w1, w2)
                        d = dis["distance"]
                        if d <= 3:
                            if len(w1) > len(w2):
                                entry[0].append(w1)
                            else:
                                entry[0].append(w2)
                        found = True
                if found is True:
                    break

            if len(entry[0]) > 2:
                second_map[key] = entry
            else:
                second_map[key] = values

        maintopic_data = {}
        maintopic_data["maintopics"] = []
        maintopic_data["subdomaintopics"] = []
        data = {}
        data["topic"] = []

        added_bigrams = []
        added_trigrams = []
        count = 0
        for b in bigrams:
            if b in second_map:
                t = second_map[b][0]
                exist_similar = False
                for at in added_trigrams:
                    coincidences = 0
                    if t[0] in at:
                        coincidences += 1
                    if t[1] in at:
                        coincidences += 1
                    if t[2] in at:
                        coincidences += 1
                    if coincidences >= 2:
                        exist_similar = True
                        break

                if t not in added_trigrams and b not in added_bigrams and not exist_similar:
                    data["topic"].append(t[0] + ' ' + t[1] + ' ' + t[2])
                    added_bigrams.append(b)
                    added_trigrams.append(t)
                    count += 1

        if count < 10:
            for b in bigrams:
                if count == 10:
                    break
                if b not in added_bigrams:
                    data["topic"].append(b[0] + ' ' + b[1])
                    added_bigrams.append(b)
                    count += 1
        maintopic_data["maintopics"] = data["topic"]
        maintopic_data["affiliationId"] = affiliationId
        for key, value in dictDomain.iteritems() :
            result = get_trigrams_sub_domain(key, value)
            maintopic_data["subdomaintopics"].append(result)
            break
        print(maintopic_data)
        break
        # es.index(index="main_topics", doc_type="affiliation_topics", id=id_aff ,body=maintopic_data)
